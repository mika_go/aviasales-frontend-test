export interface TicketModel {
  price: number;
  carrier: string;
  segments: SegmentModel[];
}

export interface SegmentModel {
  origin: string;
  destination: string;
  date: string;
  stops: string[];
  duration: number;
}

export enum FilterParam {
  All,
  NoStops,
  OneStop,
  TwoStop,
  ThreeStop
}
