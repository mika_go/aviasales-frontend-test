import {action, IObservableArray, observable, observe} from "mobx";
import {FilterParam, TicketModel} from "./types";
import axios from "axios";

class TicketsStore {
  @observable searchID: string = "";
  ticketsTemp: TicketModel[] = [];
  @observable tickets: TicketModel[] = [];
  @observable filters = new Set<FilterParam>();
  @observable isMoreCheap: boolean = false;
  @observable isLoading: boolean = false;

  constructor() {
    this.startSearch();
    observe(this.filters, _ => {
      this.startSearch();
    });

    observe(this, "isMoreCheap", _ => {
      this.startSearch();
    });
  }

  startSearch = () => {
    this.isLoading = true;
    this.ticketsTemp = [];
    this.fetchSearchID().then(() => {
      this.fetchTickets();
    });
  };

  @action
  fetchSearchID = async () => {
    const value = await axios.get("https://front-test.beta.aviasales.ru/search");
    this.searchID = value.data.searchId;
  };

  @action
  toggleFilterParam = (param: FilterParam, add: boolean) => {
    if (add) {
      this.filters.add(param);
    } else {
      this.filters.delete(param);
    }
  };

  @action
  fetchTickets = () => {
    axios.get("https://front-test.beta.aviasales.ru/tickets?searchId=" + this.searchID).then(value => {
      const {tickets, stop} = value.data;
      this.ticketsTemp.push(...tickets.filter(this.filterFunction));
      if (!stop) {
        this.fetchTickets();
        return;
      }
      (this.tickets as IObservableArray).replace(this.ticketsTemp.sort(this.sortFunction));
      this.isLoading = false;
    }).catch(reason => {
      if (!reason.response) {
        (this.tickets as IObservableArray).replace(this.ticketsTemp.sort(this.sortFunction));
        this.isLoading = false;
        return;
      }
      const status = reason.response.status;
      if (status === 500) {
        this.fetchTickets();
        return;
      }
      (this.tickets as IObservableArray).replace(this.ticketsTemp.sort(this.sortFunction));
      this.isLoading = false;
    });
  };

  filterFunction = (ticket: TicketModel): boolean => {
    const isAll = this.filters.has(FilterParam.All);
    const isNoSegment = this.filters.has(FilterParam.NoStops);
    const isOneSegment = this.filters.has(FilterParam.OneStop);
    const isTwoSegment = this.filters.has(FilterParam.TwoStop);
    const isThreeSegment = this.filters.has(FilterParam.ThreeStop);

    let filterArray: number[] = [];

    if (isAll) {
      filterArray = [0, 1, 2, 3];
    } else {
      if (isNoSegment) {
        filterArray.push(0);
      }

      if (isOneSegment) {
        filterArray.push(1);
      }

      if (isTwoSegment) {
        filterArray.push(2);
      }

      if (isThreeSegment) {
        filterArray.push(3);
      }
    }

    return ticket.segments.every(value => filterArray.includes(value.stops.length));
  };

  sortFunction = (a: TicketModel, b: TicketModel) => {
    if (this.isMoreCheap) return a.price - b.price;
    const timeA = a.segments.reduce((previousValue, currentValue) => previousValue + currentValue.duration, 0);
    const timeB = b.segments.reduce((previousValue, currentValue) => previousValue + currentValue.duration, 0);
    return timeA - timeB;
  }
}

export default TicketsStore;
