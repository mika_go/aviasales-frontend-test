import React, {FunctionComponent} from "react";
import styles from "./style.module.css";
import Filter from "./Filter";
import Tabs from "./Tabs";
import Tickets from "./Tickets";

const Main: FunctionComponent = () => (
  <div className={styles.main}>
    <Filter/>
    <div className={styles.content}>
      <Tabs/>
      <Tickets/>
    </div>
  </div>
);

export default Main;
