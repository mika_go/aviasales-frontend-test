import React, {Component} from "react";
import styles from "./style.module.css";
import Checkbox from "../../Checkbox";
import {observable} from "mobx";
import {FilterParam} from "../../../store/types";
import {inject, observer} from "mobx-react";
import TicketsStore from "../../../store";

interface Props {
  store?: TicketsStore;
}

interface ChecklistItem {
  name: string;
  checked: boolean;
  filterParam: FilterParam;
}

@inject("store")
@observer
class Filter extends Component<Props> {
  @observable checklist: ChecklistItem[] = [
    {
      name: "Все",
      checked: false,
      filterParam: FilterParam.All
    },
    {
      name: "Без пересадок",
      checked: false,
      filterParam: FilterParam.NoStops
    },
    {
      name: "1 пересадка",
      checked: false,
      filterParam: FilterParam.OneStop
    },
    {
      name: "2 пересадки",
      checked: false,
      filterParam: FilterParam.TwoStop
    },
    {
      name: "3 пересадки",
      checked: false,
      filterParam: FilterParam.ThreeStop
    }
  ];

  get store(): TicketsStore {
    return this.props.store!;
  }

  onItemClick = (item: ChecklistItem, index: number) => {
    this.checklist[index].checked = !item.checked;
    this.store.toggleFilterParam(item.filterParam, item.checked);
  };

  render() {
    return (
      <div className={styles.filter}>
        <span className={styles.name}>Количество пересадок</span>
        <div className={styles.checklist}>
          {
            this.checklist.map((value, index) => {
                const {name, checked} = value;
                return (
                  <div key={index} className={`${styles.checklistItem} ${checked ? styles.checklistItemHover : ""}`}
                       onClick={() => this.onItemClick(value, index)}>
                    <Checkbox className={styles.checklistCheckbox} checked={checked}/>
                    <span className={styles.checklistText}>{name}</span>
                  </div>
                );
              }
            )
          }
        </div>
      </div>
    );
  }
}

export default Filter;
