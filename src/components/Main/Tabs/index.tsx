import React, {Component} from "react";
import styles from "./style.module.css";
import {inject, observer} from "mobx-react";
import TicketsStore from "../../../store";
import {action} from "mobx";

interface Props {
  store?: TicketsStore;
}

@inject("store")
@observer
class Tabs extends Component<Props> {
  get store(): TicketsStore {
    return this.props.store!;
  }

  @action
  onTabClick = (cheap: boolean) => {
    this.store.isMoreCheap = cheap;
  };

  render() {
    const {isMoreCheap} = this.store;

    return (
      <div className={styles.tabs}>
        <div className={`${styles.tab} ${isMoreCheap ? styles.tabActive : ""}`}
             onClick={() => this.onTabClick(true)}>Самый дешевый
        </div>
        <div className={`${styles.tab} ${!isMoreCheap ? styles.tabActive : ""}`}
             onClick={() => this.onTabClick(false)}>Самый быстрый
        </div>
      </div>
    );
  }
}

export default Tabs;
