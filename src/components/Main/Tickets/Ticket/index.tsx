import React, {FunctionComponent} from "react";
import styles from "./style.module.css";
import logo from "./s7-airlines.svg";
import {TicketModel} from "../../../../store/types";
import Segment from "./Segment";

interface Props {
  ticket: TicketModel;
}

const Ticket: FunctionComponent<Props> = ({ticket: {price, segments}}) => (
  <div className={styles.ticket}>
    <div className={styles.ticketHeader}>
      <div className={styles.ticketPrice}>{price} р</div>
      <img className={styles.ticketLogo} src={logo} alt=""/>
    </div>
    <div className={styles.segments}>
      {
        segments.map((value, index) => <Segment key={index} segment={value}/>)
      }
    </div>
  </div>
);

export default Ticket;
