import {SegmentModel} from "../../../../../store/types";
import React, {FunctionComponent} from "react";
import styles from "./style.module.css";

interface Props {
  segment: SegmentModel;
}

const Segment: FunctionComponent<Props> = ({segment: {origin, destination, date, duration, stops}}) => {
  const startDate = new Date(date);
  const endDate = new Date(startDate.getTime() + duration * 60000);
  const durHours = Math.floor(duration / 60);
  const durMinutes = duration - durHours * 60;
  let stopString = "";
  if (stops.length === 1) stopString = "1 пересадка";
  else if ([2, 3, 4].includes(stops.length)) stopString = `${stops.length} пересадки`;
  else if (stops.length === 0) stopString = "нет пересадок";
  else stopString = `${stops.length} пересадок`;

  return (
    <div className={styles.segment}>
      <div className={styles.header}>{origin} - {destination}</div>
      <div className={styles.header}>В пути</div>
      <div className={styles.header}>{stopString}</div>
      <div className={styles.content}>{startDate.getHours()}:{startDate.getMinutes()} - {endDate.getHours()}:{endDate.getMinutes()}</div>
      <div className={styles.content}>{durHours}ч {durMinutes}м</div>
      <div className={styles.content}>{stops.join(", ")}</div>
    </div>
  );
};

export default Segment;
