import React, {Component} from "react";
import styles from "./style.module.css";
import {inject, observer} from "mobx-react";
import TicketsStore from "../../../store";
import Ticket from "./Ticket";

interface Props {
  store?: TicketsStore;
}

@inject("store")
@observer
class Tickets extends Component<Props> {
  get store(): TicketsStore {
    return this.props.store!;
  }

  render() {
    const {tickets} = this.store;
    const actualTickets = tickets.slice(0, 5);
    return (
      <div className={styles.tickets}>
        {
          actualTickets.map((value, index) => <Ticket key={index} ticket={value}/>)
        }
      </div>
    );
  }
}

export default Tickets;
