import React, {Component} from "react";
import MainLogo from "../MainLogo";
import styles from "./style.module.css";
import TicketsStore from "../../store";
import {inject, observer} from "mobx-react";

interface Props {
  store?: TicketsStore;
}

@inject("store")
@observer
class Header extends Component<Props> {
  get store(): TicketsStore {
    return this.props.store!;
  }

  render() {
    const {isLoading} = this.store;

    return (
      <div className={styles.header}>
        <MainLogo className={styles.logo} rotation={isLoading}/>
      </div>
    );
  }
}

export default Header;
